import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ItemEditComponent } from './shopping-list/item-edit/item-edit.component';
import { ItemDetailComponent } from './shopping-list/item-detail/item-detail.component';
import { ShoppingComponent } from './shopping-list/shopping.component';
import { ShoppingListStartComponent } from './shopping-list/shopping-list-start.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent },
    {
        path: 'list', component: ShoppingComponent, children: [
            { path: '', component: ShoppingListStartComponent },
            { path: 'new', component: ItemEditComponent },
            { path: ':id', component: ItemDetailComponent },
            { path: ':id/edit', component: ItemEditComponent }
        ]
    },
    { path: 'cart', component: ShoppingCartComponent },
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(APP_ROUTES);

