import { StoreAppState } from './../../store';
import { Item } from './../../item';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, NgRedux } from '@angular-redux/store';
import { Subscription, Observable } from 'rxjs/Rx';
import { ADD_ITEM, EDIT_ITEM, DELETE_ITEM, ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART } from '../../actions';

@Component({
  selector: 'sa-item-detail',
  templateUrl: './item-detail.component.html',
  styles: []
})
export class ItemDetailComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @select() items: Observable<Item[]>;
  selectedItem: Item;



  constructor(private route: ActivatedRoute, private router: Router, private ngRedux: NgRedux<StoreAppState>) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe((params) => {
      const id: number = +params['id'];
      this.items.subscribe((itemsList) => {
        this.selectedItem = itemsList.find(t => t.id === id);
      });
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  addToCart() {
    this.ngRedux.dispatch({ type: ADD_ITEM_TO_CART, id: this.selectedItem.id });
  }

  removeFromCart() {
    this.ngRedux.dispatch({ type: REMOVE_ITEM_FROM_CART, id: this.selectedItem.id });
  }

  onDelete() {
    this.ngRedux.dispatch({ type: DELETE_ITEM, id: this.selectedItem.id });
    this.router.navigate(['/list']);
  }

  onEdit() {
    this.router.navigate(['/list', this.selectedItem.id, 'edit']);
  }

}
