import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sa-shopping-list-start',
  template: `
    <h2>Please select an item</h2>
  `,
})
export class ShoppingListStartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
