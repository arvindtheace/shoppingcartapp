import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sa-home',
  template: `
    <h3>
      Welcome to the Shopping App. Click on <strong>List</strong> tab to see list of all items available
      and add them to your cart.
    </h3>
  `,
  styles: []
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
