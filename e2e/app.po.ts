import { browser, element, by } from 'protractor';

export class ShoppingAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('sa-root h1')).getText();
  }
}
