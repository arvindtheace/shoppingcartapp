export class Item {
    constructor(
        public id: number,
        public name: string,
        public description: string,
        public categoryId: number,
        public categoryName: string,
        public addedToCart: boolean
    ) {

    }
}
