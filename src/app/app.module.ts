
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, isDevMode } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { StoreAppState, rootReducer, INITIAL_STATE } from './store';

import { routing } from './app.routing';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingListStartComponent } from './shopping-list/shopping-list-start.component';
import { ShoppingComponent } from './shopping-list/shopping.component';
import { ItemDetailComponent } from './shopping-list/item-detail/item-detail.component';
import { ItemEditComponent } from './shopping-list/item-edit/item-edit.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ShoppingListComponent,
    ShoppingListStartComponent,
    ShoppingComponent,
    ItemDetailComponent,
    ItemEditComponent,
    ShoppingCartComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    NgReduxModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private ngRedux: NgRedux<StoreAppState>, private devTools: DevToolsExtension) {
    const enhancers = isDevMode() ? [devTools.enhancer()] : [];
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);

  }
}
