import { REMOVE_ITEM_FROM_CART } from './../actions';
import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';

import { StoreAppState } from './../store';

@Component({
  selector: 'sa-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  @select() cartCount;
  @select() items;
  constructor(private ngRedux: NgRedux<StoreAppState>) { }

  ngOnInit() {
  }
  removeItem(id) {
    this.ngRedux.dispatch({ type: REMOVE_ITEM_FROM_CART, id: id });
  }
  checkoutItem(id) {
    const choice = confirm(
      'This action will remove the item from the cart.' +
      'As this is a demo, there is no separate tracking view. \n Do you want to continue?');
    if (choice) {
      this.ngRedux.dispatch({ type: REMOVE_ITEM_FROM_CART, id: id });
    }
  }
}
