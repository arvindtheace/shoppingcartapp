import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgRedux, select } from '@angular-redux/store';
import { Subscription, Observable } from 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Item } from './../../item';
import { StoreAppState } from './../../store';
import { EDIT_ITEM, ADD_ITEM } from './../../actions';
@Component({
  selector: 'sa-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css']
})
export class ItemEditComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  @select() items: Observable<Item[]>;
  selectedItem: Item;
  isNew: boolean;
  itemForm: FormGroup;
  id: number;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private ngRedux: NgRedux<StoreAppState>) {
  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe((params) => {
      if (params.hasOwnProperty('id')) {
        this.isNew = false;
        this.id = +params['id'];
        this.items.subscribe((itemsList) => {
          this.selectedItem = itemsList.find(t => t.id === this.id);
        });
      } else {
        this.isNew = true;
      }
    });
    this.initForm();
  }

  initForm() {
    if (!this.isNew) {
      this.itemForm = this.formBuilder.group({
        name: [this.selectedItem.name, Validators.required],
        description: [this.selectedItem.description, Validators.required],
        category: [this.selectedItem.categoryName, Validators.required]
      });
    } else {
      this.itemForm = this.formBuilder.group({
        name: ['', Validators.required],
        description: ['', Validators.required],
        category: ['', Validators.required]
      });
    }

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit() {
    if (this.isNew) {
      this.ngRedux.dispatch({ type: ADD_ITEM, object: this.itemForm.value });
    } else {
      this.ngRedux.dispatch({ type: EDIT_ITEM, id: this.id, object: this.itemForm.value });
    }
    this.router.navigate(['list']);
  }

  private navigateBack() {
    this.router.navigate(['list']);
  }

}
