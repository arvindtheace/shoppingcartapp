import { Item } from './item';
import { ADD_ITEM, EDIT_ITEM, DELETE_ITEM, ADD_ITEM_TO_CART, REMOVE_ITEM_FROM_CART } from './actions';
import { tassign } from 'tassign';

export interface StoreAppState {
    items: Item[];
    cartCount: number;
    index: number;
};

export const INITIAL_STATE: StoreAppState = {
    // ideally this should be fetched from a server using a http service
    items: [
        new Item(0, 'ABC Shoes', 'Comfortable shoes for running', 1, 'shoes', true),
        new Item(1, 'XYZ Shirts', 'Cotton shirts for everyday use', 2, 'shirts', false),
        new Item(2, 'FOO Pants', 'Leisure pants for casual wear', 3, 'pants', false)
    ],
    cartCount: 1,
    index: 2
};

export function rootReducer(state: StoreAppState, action): StoreAppState {
    switch (action.type) {
        case ADD_ITEM_TO_CART:

            const selecteditem: Item = state.items.find(t => t.id === action.id);
            selecteditem.addedToCart = true;
            const itemIndex = state.items.indexOf(selecteditem);

            return tassign(state, {
                items:
                [...state.items.slice(0, itemIndex),
                    selecteditem,
                ...state.items.slice(itemIndex + 1)
                ],
                cartCount: state.cartCount + 1
            });
        case REMOVE_ITEM_FROM_CART:
            const item: Item = state.items.find(t => t.id === action.id);
            item.addedToCart = false;
            const index = state.items.indexOf(item);

            return tassign(state, {
                items:
                [...state.items.slice(0, index),
                    item,
                ...state.items.slice(index + 1)
                ],
                cartCount: state.cartCount - 1
            });

        case DELETE_ITEM:
            const itemToDelete: Item = state.items.find(t => t.id === action.id);
            let cartCount = state.cartCount;
            if (itemToDelete.addedToCart) {
                cartCount = state.cartCount - 1;
            }
            return tassign(state, {
                items: state.items.filter(i => i.id !== action.id),
                cartCount: cartCount
            });
        case EDIT_ITEM:

            const itemToEdit: Item = state.items.find(t => t.id === action.id);
            const editIndex = state.items.indexOf(itemToEdit);
            itemToEdit.categoryName = action.object.category;
            itemToEdit.description = action.object.description;
            itemToEdit.name = action.object.name;

            return tassign(state, {
                items:
                [...state.items.slice(0, editIndex),
                    itemToEdit,
                ...state.items.slice(editIndex + 1)
                ]
            });
        case ADD_ITEM:
            const newItem = new Item(
                state.index + 1,
                action.object.name,
                action.object.description,
                2,
                action.object.category,
                false);
            return tassign(state, {
                items: state.items.concat(newItem),
                index: state.index + 1
            });
    }
    return state;
}


