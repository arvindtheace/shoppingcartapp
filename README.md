# ShoppingApp

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

Please do a `npm install` before starting the server.
This project is using redux as its state management. Find the related code in store.ts

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

You can choose the port to deploy by using `ng serve --port port` option